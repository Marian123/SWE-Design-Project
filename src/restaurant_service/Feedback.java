package restaurant_service;

public class Feedback {
	private int rating;
	private String content;
	
	public Feedback(int rating, String content) {
		this.rating = rating;
		this.content = content;
	}
	
}
