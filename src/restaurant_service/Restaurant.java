package restaurant_service;

import java.util.ArrayList;
import java.util.Iterator;

public class Restaurant {
	private String name;
	private String Address;
	private Address address;
	private String phone_number;
	private OpeningHours opening_hours;
	private int typeOfSubscription;
	private Menu menu;
	private ArrayList<Feedback> feedbacks;
	private ArrayList<Order> all_orders;
	
	public Restaurant(String name, int typeOfSubscription, String phone_Number, Address address, OpeningHours opening_hours, Menu menu){
		this.name = name;
		this.typeOfSubscription = typeOfSubscription;
		this.phone_number = phone_Number;
		this.Address = Address;
		this.opening_hours = opening_hours;
		this.menu = menu;
		feedbacks = new ArrayList<Feedback>();
		all_orders = new ArrayList<Order>();
	}
	
	public Restaurant(String name, int typeOfSubscription, String phone_Number, Address address, OpeningHours opening_hours){
		this.name = name;
		this.typeOfSubscription = typeOfSubscription;
		this.phone_number = phone_Number;
		this.Address = Address;
		this.opening_hours = opening_hours;
		feedbacks = new ArrayList<Feedback>();
		all_orders = new ArrayList<Order>();
	}

	public Menu getMenu() {
		/*Idealerweise sollte man hier erst checken ob
		 * das restaurant ein Menue hat*/
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}
	
	public void addOrder(Order order){
		/*idealerweise k�nnte man hier erst gucken ob die Order
		 * nicht schon enthalten ist und boolean zur�ckgeben*/
		all_orders.add(order);
	}
	
	public void removeOrder(Order order){
		if(all_orders.contains(order)){
			all_orders.remove(order);
		}
	}
	
	public void giveFeedback(Feedback feedback){
		this.feedbacks.add(feedback);
	}
	
	public String getName(){
		return name;
	}
	
}
