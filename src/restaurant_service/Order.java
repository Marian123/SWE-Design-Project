package restaurant_service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Order {
	private Date date;
	private Restaurant restaurant;
	private Customer customer;
	private Address deliveryAddress;
	private int payment;
	private ArrayList<PersonalOrder> orders;
	private String status;
	
	public Order(Restaurant restaurant, Address deliveryAddress, Customer customer, ArrayList<PersonalOrder> orders){
		this.orders = orders;
		status = "Waiting for the customer to pay";
		date = new Date();
		this.customer = customer;
		this.orders = orders;
	}
	
	public void addPersOrder(PersonalOrder order){
		orders.add(order);
	}
}
