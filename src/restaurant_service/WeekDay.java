package restaurant_service;

//0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday
public class WeekDay {
	private float openingHour;
	private float closingHour;
	private boolean closed;
	
	public WeekDay(float openingHour, float closingHour){
		this.openingHour = openingHour;
		this.closingHour = closingHour;
		closed = false;
	}
	
	public WeekDay(){
		this.openingHour = 0;
		this.closingHour = 0;
		closed = true;
	}
	
	public boolean isOpen(float time){
		if(!closed){
			if(time>=openingHour && time<= closingHour){
				return true;
			}
		}
		return false;
	}
	
	public float getOpeningHour(){
		return openingHour;
	}
	
	public float getClosingHour(){
		return closingHour;
	}

	public void setOpeningHour(float openingHour) {
		this.openingHour = openingHour;
	}

	public void setClosingHour(float closingHour) {
		this.closingHour = closingHour;
	}
	
}
