package restaurant_service;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class OpeningHours {
	public WeekDay[] weekDays; //0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday
	public ArrayList<Holidays> holidays;
	
	public OpeningHours(WeekDay[] weekDays, Holidays holidays){
		this.holidays = new ArrayList<Holidays>();
		this.holidays.add(holidays);
		this.weekDays = weekDays;
	}
	
	public void setHoliday(ArrayList<Holidays> holidays){
		this.holidays = holidays;
	}
	
	public boolean isOpen(Date date){
		Iterator<Holidays> it = holidays.iterator();
		while(it.hasNext()){
			Holidays tmp = it.next();
			if(tmp.hasHolidays(date) == true){
				return false;
			}
		}
		//date.getDay() returns the values (0 = Sunday, 1 = Monday, 2 = Tuesday, 3 = Wednesday, 4 = Thursday, 5 = Friday, 6 = Saturday)
		if(weekDays[date.getDay()].isOpen((float)(date.getHours() + (date.getMinutes()/100f))) == false){
			return false;
		}
		return true;
	}
	
	public static void main(String[]args){
		//Example to test it
		WeekDay[] weekDays = new WeekDay[7];
		weekDays[0] = new WeekDay();
		for(int i=1;i<6;i++){
			weekDays[i] = new WeekDay(8.00f, 18.00f);
		}
		weekDays[6] = new WeekDay(8.00f,14.00f);
		Holidays holis = new Holidays(new Date(2017,1,11), new Date(2017,1,16));
		OpeningHours openingHours  = new OpeningHours(weekDays,holis);
		Date date = new Date(2017,6,5);
		date.setHours(14);
		date.setMinutes(13);
		System.out.println(openingHours.isOpen(date));
		
	}
}
