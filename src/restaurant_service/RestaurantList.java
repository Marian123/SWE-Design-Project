package restaurant_service;

import java.util.ArrayList;

public class RestaurantList {
	
	private ArrayList<Restaurant> restaurants;
	
	public RestaurantList(){
		restaurants = new ArrayList<Restaurant>();
	}
	
	public void addRestaurant(Restaurant restaurant){
		/*Idealerweise k�nnte man hier noch checken ob das 
		 * restaurant nicht schon in restaurants existiert 
		 * und eine boolean zur�ckgeben*/
		restaurants.add(restaurant);
	}
	
	public void removeRestaurant(Restaurant restaurant){
		/*Idealerweise sollte man hier erst checken ob 
		 * das restaurant in restaurants existiert*/
		restaurants.remove(restaurant); 
	}
	
	public void search(int zipcode){}

}
