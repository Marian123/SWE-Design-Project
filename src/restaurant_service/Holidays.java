package restaurant_service;

import java.util.Date;

public class Holidays {
	public Date startDate;
	public Date endDate;
	
	public Holidays(Date startDate, Date endDate){
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public boolean hasHolidays(Date date){
		if(startDate.compareTo(date) == 0 || endDate.compareTo(date) == 0){
			return true;
		}
		else if(startDate.compareTo(date) < 0 && endDate.compareTo(date) > 0){
			return true;
		}
		else 
			return false;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public static void main(String[] args){
		Holidays ho = new Holidays(new Date(2017, 1, 10), new Date(2017,1,20));
		System.out.println(ho.hasHolidays(new Date(2017,1,29)));  
	}
}
