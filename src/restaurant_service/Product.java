package restaurant_service;

public class Product {
	private int prod_number;
	private String name;
	private double price;
	
	public Product(int prod_number, String name, double price){
		this.prod_number = prod_number;
		this.name = name;
		this.price = price;
	}
	
	public Product selectingProduct(){
		return this;
	}

	public int getProd_number() {
		return prod_number;
	}

	public void setProd_number(int prod_number) {
		this.prod_number = prod_number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	


}
