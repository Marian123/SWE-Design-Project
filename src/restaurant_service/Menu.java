package restaurant_service;

import java.util.ArrayList;

public class Menu {
	private ArrayList<Product> products;
	
	public Menu(){
		products = new ArrayList<Product>();
	}
	
	public void addProduct(Product product){
		/*Man k�nnte hier auch noch erst checken ob das Produkt nicht
		 * schon in products vorhanden ist*/
		products.add(product);
	}
	
	public boolean remProduct(Product product){
		if(products.contains(product)){
			products.remove(product);
			return true;
		}
		else return false;
	}
	
	public ArrayList<Product> getList(){
		return products;
	}
}
