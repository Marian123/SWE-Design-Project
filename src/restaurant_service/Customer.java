package restaurant_service;

import java.util.ArrayList;
import java.util.Scanner;

public class Customer {
	private String name;
	private Address address;
	private double credits;
	private String email;
	private String password;
	private ArrayList<Order> orderList;
	
	public Customer(String name, Address address, String email, String password) {
		this.name = name;
		this.address = address;
		this.email = email;
		this.password = password;
	}
	
	//zum vereinfachen -> macht eigentlich nicht so viel sinn
	public Customer(String name){
		this.name = name;
	}
	
	public void addOrder(Order order){}
	public void addCredits(){}
	public void giveFeedback(Restaurant restaurant, String content, int rating){
		Feedback feedback = new Feedback(rating,content);
		restaurant.giveFeedback(feedback);
	}
	
	public Address getAddress(){
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
}
