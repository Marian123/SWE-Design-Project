package restaurant_service;

public class Address {
	private String street;
	private String city;
	private int zipcode;
	private String country;
	
	public Address(String street, String city, int zipcode, String country){
		this.street = street;
		this.city = city;
		this.zipcode = zipcode;
		this.country = country;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String address) {
		this.street = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getZipcode() {
		return zipcode;
	}
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
}
