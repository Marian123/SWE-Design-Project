package restaurant_service;

public class PersonalOrder {
	private Product product;
	private int quantity;
	
	public PersonalOrder(Product product, int quantity){
		this.product = product;
		this.quantity = quantity;
	}
	
	public PersonalOrder(Product product){
		this.product = product;
		this.quantity = 1;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
