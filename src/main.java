import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import restaurant_service.*;
public class main {

	public static void main(String[] args) {
//Creating the Content
		//OpeningHours
		WeekDay[] weekDays = new WeekDay[7];
		weekDays[0] = new WeekDay();
		for(int i=1;i<6;i++){
			weekDays[i] = new WeekDay(8.00f, 18.00f);
		}
		weekDays[6] = new WeekDay(8.00f,14.00f);
		Holidays holis = new Holidays(new Date(2017,1,11), new Date(2017,1,16));
		OpeningHours open_hours1 = new OpeningHours(weekDays,holis);
		OpeningHours open_hours2 = new OpeningHours(weekDays,holis);
		//Addresses
		Address address1 = new Address("mystreet1","myCity1",03046,"Germany");
		Address address2 = new Address("mystreet2","myCity2",01043,"Germany");
		Address address3 = new Address("mystreet3","myCity3",01443,"Germany");
		//Products
		Product pizzaHawaii = new Product(1,"Pizza Hawaii",8.55);
		Product pizzaSalami = new Product(2,"Pizza Salami",8.55);
		Product pizzaThunfisch = new Product(2,"Pizza Thunfisch",8.55);
		Product hamburger = new Product(3,"Hamburger",5.99);			
		//Menues
		Menu menu1 = new Menu();
		menu1.addProduct(pizzaHawaii);
		menu1.addProduct(pizzaSalami);
		menu1.addProduct(pizzaThunfisch);
		
		Menu menu2 = new Menu();
		menu2.addProduct(pizzaHawaii);
		menu2.addProduct(pizzaSalami);
		menu2.addProduct(pizzaThunfisch);
		menu2.addProduct(hamburger);
		//Restaurants
		Restaurant restaurant1 = new Restaurant("myRestaurant1",1, "098765433212", address1, open_hours1, menu1);
		Restaurant restaurant2 = new Restaurant("myRestaurant2",1, "123456787780", address2, open_hours2, menu2);
		Restaurant[] restaurants = new Restaurant[2];
		restaurants[0] = restaurant1;
		restaurants[1] = restaurant2;
		//Customer
		Customer customer = new Customer("Horst",address3,"horst@web.de","myPassword");
	
//THE PROGRAM
//Asking what to do
		boolean check=false;
		int whatToDo = -1;
		while(!check){
			System.out.println("What do you want to do?");
			System.out.println("1: Change your Address");
			System.out.println("2: Order something");
			Scanner myscanner = new Scanner(System.in);
			String inp = myscanner.nextLine();
			try{
				whatToDo = Integer.parseInt(inp);
				check = true;
			}catch(Exception e){System.out.println("Wrong input try again");}			
		}
		if(whatToDo == 1){
//Enter an address
			System.out.println("Please enter your Street");
			Scanner reader = new Scanner(System.in);
			String new_street = reader.nextLine();
			System.out.println("Please enter your city");
			String new_city = reader.nextLine();
			System.out.println("Please enter your country");
			String new_country = reader.nextLine();
			check=false;
			int zipcode = 0;
			while(!check){
				System.out.println("Please enter your zipcode");
				String zip = reader.nextLine();
				try{
					zipcode = Integer.parseInt(zip);
					check=true;
				}catch(Exception e){System.out.println("Wrong input try again");}
			}
			customer.setAddress(new Address(new_street,new_city,zipcode,new_country));
			System.out.println("Your Address has been changed");
		}
		else if(whatToDo == 2){
//PICKING A DISH/RESTAURANT		
//Picking a restaurant
			System.out.println("Which Restaurant do you want to order from:");
			int chosenRestaurant = 0;
			while(chosenRestaurant==0){
				System.out.println("1: " + restaurant1.getName());
				System.out.println("2: " + restaurant2.getName());
				Scanner scanner = new Scanner(System.in);
				String input = scanner.nextLine();
				try{
					chosenRestaurant = Integer.parseInt(input);
					if(chosenRestaurant<0 || chosenRestaurant>2){
						chosenRestaurant=0;
						throw new Exception();
					}
				}catch(Exception e){System.out.println("Wrong Input try again");}
			}
			System.out.println("You chose " + restaurants[chosenRestaurant-1].getName());
//Picking a dish
			System.out.println("Type in the appropriate number to pick a dish:");
			int chosenDish=0;
			Menu chosenMenu = restaurants[chosenRestaurant-1].getMenu();
			ArrayList<Product> chosenProducts = chosenMenu.getList();
			while(chosenDish==0){
				for(int i=0;i<chosenProducts.size();i++){
					System.out.println( (i+1) + ": " + chosenProducts.get(i).getName());
				}
				Scanner scanner = new Scanner(System.in);
				String input = scanner.nextLine();
				try{
					chosenDish = Integer.parseInt(input);
					if(chosenDish<0 || chosenDish>chosenProducts.size()){
						chosenDish=0;
						throw new Exception();
					}
				}catch(Exception e){System.out.println("Wrong Input try again");}
			}
			System.out.println("You picked " + chosenProducts.get(chosenDish-1).getName());
			ArrayList<PersonalOrder> p_orders = new ArrayList<PersonalOrder>();
			p_orders.add(new PersonalOrder(chosenProducts.get(chosenDish-1)));
			Order order = new Order(restaurants[chosenRestaurant -1],customer.getAddress(),customer,p_orders);
			customer.addOrder(order);
			System.out.println("Your request has been accepted");
		}	
	}

}
